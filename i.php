<?php
/*
 *
 * Author LineGraphs
 *
*/
// настройки
$x0 = 600; // абсцисса точки начала отсчёта
$y0 = 350; // ордината точки начала отсчёта
$ks = 0.01; // точность рисования (бОльшая точность - меньшая скорость)
$m = 5; // масштаб (1|2.5|5)

function grafiki ($x) { // функция, график которой рисуем
	// $x - значение аргумента функции
	// возвращаемое значение - значение функции от данного аргумента
	// поддерживаются все математические PHP-функции
	return 2*($x)+1;
}
// начало программы
set_time_limit(90);
$m = $m * 10;
// создаём изображение
$img = imagecreate($x0 * 2 + 1, $y0 * 2 + 1);
// настраиваем цвета
$bgColor = ImageColorAllocate($img, 245, 245, 245); // цвет фона
$lnColor = ImageColorAllocate($img, 230, 230, 230); // цвет сетки
$odColor = ImageColorAllocate($img, 0, 0, 0); // цвет осей
$grColor = ImageColorAllocate($img, 0, 0, 255); // цвет графика
// фон
ImageFill($img, 0, 0, $bgColor);
// рисуем координатную сетку
for ($i = 0; $i <= $x0 * 2; $i++) {
	// линии
	ImageLine($img, $i * $m, 0, $i * $m, $y0 * 2, $lnColor);
	ImageLine($img, 0, $i * $m, $x0 * 2, $i * $m, $lnColor);
	// чёрточки
	ImageLine($img, $i * $m, $y0 - 2, $i * $m, $y0 + 2, $odColor);
	ImageLine($img, $x0 - 2,  $i * $m, $x0 + 2, $i * $m, $odColor);
}
// рисуем циферки
for ($i = 0; $i <= $x0 * 2; $i++) {
	if ($i - ($x0 / $m) != 0) imagettftext($img, 6, 270, $i * $m - 1, $y0 + 5, $odColor, "arial.ttf", $i - ($x0 / $m));
	if (- ($i - ($y0 / $m)) != 0) imagettftext($img, 6, 0, $x0 + 5, $i * $m + 2, $odColor, "arial.ttf", - ($i - ($y0 / $m)));
}
// рисуем координатные оси
ImageLine($img, $x0, 0, $x0, $y0 * 2, $odColor);
ImageLine($img, 0, $y0, $x0 * 2, $y0, $odColor);
// рисуем график функции
for ($x = -$x0; $x < $x0; $x += $ks) {
	if (
		/* ФУНКЦИЯ, начало */
			$y = grafiki($x)
		/* ФУНКЦИЯ, конец */
	) {
		if ($x >= 0) $d = 1;
		else $d = 0;
		$qx = $x * $m;
		$y = $y * $m;
		ImageLine($img, $x0 + $d + $qx, $y0 - $y, $x0 + $d + $qx, $y0 - $y, $grColor);
	}
}
// пишем копирайт
imagettftext($img, 11, 0, 9, 20, $odColor, "times.ttf", "Рисуем Графики");
imagettftext($img, 11, 0, 9, 40, $odColor, "arial.ttf", "y = 2x+1");
// выводим изображение в браузер
header("Content-type: image/png");
ImagePng($img);
ImageDestroy($img);
// завершаем программу
exit();
?>